<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Testv extends CI_Controller {

	public function index()
	{
		$this->load->view('header');
		$this->load->view('view/notice_list');
		$this->load->view('footer');
	}
	public function assignment()
	{
		# code...
		$this->load->view('header');
		$this->load->view('admin/b_assigment');
		$this->load->view('footer');
	}
	public function notice_view()
	{
		# code...
		if($this->input->get('notice_id')!= NULL)
		{
			$data['notice_id'] = $this->input->get('notice_id');
			$this->load->view('header');
			$this->load->view('view/notice_view', $data);
			$this->load->view('footer');
		}
	}
	public function notice_update()
	{
		# code...
		if($this->input->get('notice_id')!= NULL)
		{
			$data['notice_id'] = $this->input->get('notice_id');
			$this->load->view('header');
			$this->load->view('view/notice_update', $data);
			$this->load->view('footer');
		}
	}
	public function notice_update_process()
	{
		$this->form_validation->set_rules('n_id', 'n_id', 'required');
		$this->form_validation->set_rules('n_title', 'n_title', 'required');
		$this->form_validation->set_rules('n_body', 'n_body', 'required');

		if ($this->form_validation->run() == TRUE) {
			# code...
			if($this->input->post('n_id')!=NULL)
			{
				$data = array(
							'n_title' =>$this->input->post('n_title'),
							'n_body'  =>$this->input->post('n_body')
							
							
						);	
				    $n_id = $this->input->post('n_id');
					$this->db->where('n_id', $n_id);
					$this->db->update('notice', $data);
					redirect('testv/index','refresh');
			}
		} else {
			# code...
		}

	}

}

/* End of file testv.php */
/* Location: ./application/controllers/testv.php */