<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
	function __construct()
	{	
			parent::__construct();
			$this->load->library('form_validation');
			$this->load->helper('form');
			$this->load->library('session');
			$this->load->model('getdb');
	}

	public function index()
	{
		$this->load->view('header');
		$this->load->view('admin/b_addexammarks');
		$this->load->view('footer');		
	}
	public function exam()
	{
		# code...
		if (isset($this->session->userdata['login_data'])) {
		$this->load->view('header');
		$this->load->view('admin/b_addexam');
		$this->load->view('footer');
		}
		else{
			redirect('login/index','refresh');
		}
	}
	public function addexam()
	{
		if (isset($this->session->userdata['login_data'])) {
		if($this->input->post('submit')){
		$this->form_validation->set_rules('exam_name', 'exam_name', 'required');
		$this->form_validation->set_rules('class_name', 'class_name', 'required');

		$this->form_validation->set_rules('t_marks', 't_marks', 'required');
		$this->form_validation->set_rules('exam_date', 'exam_date', 'required');
		

		if ($this->form_validation->run() == TRUE ) {
			# code...
			$date= date("d-M-Y");
			//$data['n_date']= $date; 
			$data = array(
							'pub_date' =>$date,
							'exam_name'  =>$this->input->post('exam_name'),
							'class_id' =>$this->input->post('class_name'),
							'total_marks'=>$this->input->post('t_marks'),
							'date'=>$this->input->post('exam_date')
							
							
							
						);
			$this->db->insert('exam', $data);
			
			$data['mess']='Sucessfully Enter exam';
			$this->load->view('header');
			$this->load->view('admin/b_addexam',$data);
			$this->load->view('footer');

			
			#work-check in nect phase

		} else {
			# Notice View
			$data['mess']='form validation error';
			$this->load->view('header');
			$this->load->view('admin/b_addexam',$data);
			$this->load->view('footer');
		}
	}
	else{
		# code...
		# Notice View
		$data['mess']='Post error';
			$this->load->view('header');
			$this->load->view('admin/b_addexam',$data);
			$this->load->view('footer');
	}
		




}
else{
			redirect('login/index','refresh');
		}
	}
	public function exammarks()
	{
		# code...
		if (isset($this->session->userdata['login_data'])) {
		$this->load->view('header');
		$this->load->view('admin/b_addexammarks');
		$this->load->view('footer');
	}
	else{
			redirect('login/index','refresh');
		}
	}
	public function addexammarks()
	{
		if (isset($this->session->userdata['login_data'])) {
		if($this->input->post('submit')){
		$this->form_validation->set_rules('exam_name', 'exam_name', 'required');
		

		$this->form_validation->set_rules('marks', 'marks', 'required');
		$this->form_validation->set_rules('s_id', 's_id', 'required');
		

		if ($this->form_validation->run() == TRUE ) {
			# code...
			$date= date("d-M-Y");
			//$data['n_date']= $date; 
			$data = array(
							'pub_date' =>$date,
							'exam_id'  =>$this->input->post('exam_name'),
							
							'o_marks'=>$this->input->post('marks'),
							'student_id'=>$this->input->post('s_id')
							
							
							
						);
			$this->db->insert('exam_marks', $data);
			
			$data['mess']='Sucessfully Enter marks';
			$this->load->view('header');
			$this->load->view('admin/b_addexammarks',$data);
			$this->load->view('footer');

			
			#work-check in nect phase

		} else {
			# Notice View
			$data['mess']='form validation error';
			$this->load->view('header');
			$this->load->view('admin/b_addexammarks',$data);
			$this->load->view('footer');
		}
	}
	else{
		# code...
		# Notice View
		$data['mess']='Post error';
			$this->load->view('header');
			$this->load->view('admin/b_addexammarks',$data);
			$this->load->view('footer');
	}
		




   }
   else{
			redirect('login/index','refresh');
		}
	}
	public function assignment()
	{
		if (isset($this->session->userdata['login_data'])) {
		$this->load->view('header');
		$this->load->view('admin/b_assigment');
		$this->load->view('footer');
	}
	else{
			redirect('login/index','refresh');
		}
	}
	public function add_ass()
	{
		# code...
		if (isset($this->session->userdata['login_data'])) {
		if($this->input->post('submit')){
		$this->form_validation->set_rules('sub_name', 'sub_name', 'required');
		$this->form_validation->set_rules('class_name', 'class_name', 'required');

		$this->form_validation->set_rules('ass_title', 'ass_title', 'required');
		$this->form_validation->set_rules('ass_date', 'ass_date', 'required');
		$this->form_validation->set_rules('ass_link', 'ass_link', 'required');

		if ($this->form_validation->run() == TRUE ) {
			# code...
			$date= date("d-M-Y");
			//$data['n_date']= $date; 
			$data = array(
							'pub_date' =>$date,
							'class_id'  =>$this->input->post('class_name'),
							'sub_id' =>$this->input->post('sub_name'),
							'ass_title'=>$this->input->post('ass_title'),
							'ass_link'=>$this->input->post('ass_link'),
							'last_date'=>$this->input->post('ass_date')
							
							
						);
			$this->db->insert('assignment', $data);
			
			$data['mess']='Sucessfully Enter assignment';
			$this->load->view('header');
			$this->load->view('admin/b_assigment',$data);
			$this->load->view('footer');

			
			#work-check in nect phase

		} else {
			# Notice View
			$data['mess']='form validation error';
			$this->load->view('header');
			$this->load->view('admin/b_assigment',$data);
			$this->load->view('footer');
		}
	}
	else{
		# code...
		# Notice View
		$data['mess']='Post error';
			$this->load->view('header');
			$this->load->view('admin/b_assigment',$data);
			$this->load->view('footer');
	}
		

 	}
 	else{
			redirect('login/index','refresh');
		}
	}
	public function assdel()
	{
		# code...
		if (isset($this->session->userdata['login_data'])) {
		if($this->input->get('submit')){
			
			if($this->input->get('delete')!=NULL)
			{
				$data=$this->input->get('delete');
					$this->db->where('ass_id', $data);
					$this->db->delete('assignment');
					redirect('Testv/index','refresh');
			}
			else{
				echo "Get not found";
			}

			

		}
		else
		{
			echo "Post error";
		}
	}
	else{
			redirect('login/index','refresh');
		}
	}
	public function subject()
	{
		if (isset($this->session->userdata['login_data'])) {
		$this->load->view('header');
		$this->load->view('admin/b_subject');
		$this->load->view('footer');
	}
	else{
			redirect('login/index','refresh');
		}
		
	}
	public function add_sub()
	{
		if (isset($this->session->userdata['login_data'])) {
		if($this->input->post('submit')){
		$this->form_validation->set_rules('sub_name', 'sub_name', 'required');
		$this->form_validation->set_rules('class_name', 'class_name', 'required');

		$this->form_validation->set_rules('teacher_name', 'teacher_name', 'required');

		if ($this->form_validation->run() == TRUE ) {
			# code...
			
			//$data['n_date']= $date; 
			$data = array(
							'name' =>$this->input->post('sub_name'),
							'class_id'  =>$this->input->post('class_name'),
							'teacher_id' =>$this->input->post('teacher_name')
							
							
						);
			$this->db->insert('subject', $data);
			
			$data['mess']='Sucessfully Enter subject';
			$this->load->view('header');
			$this->load->view('admin/b_subject',$data);
			$this->load->view('footer');

			
			#work-check in nect phase

		} else {
			# Notice View
			$data['mess']='form validation error';
			$this->load->view('header');
			$this->load->view('admin/b_subject',$data);
			$this->load->view('footer');
		}
	}
	else{
		# code...
		# Notice View
		$data['mess']='Post error';
			$this->load->view('header');
			$this->load->view('admin/b_subject',$data);
			$this->load->view('footer');
	}
		
}
else{
			redirect('login/index','refresh');
		}
	}
	public function teacher()
	{
			if (isset($this->session->userdata['login_data'])) {
			$this->load->view('header');
			$this->load->view('admin/b_teacher');
			$this->load->view('footer');
		}
		else{
			redirect('login/index','refresh');
		}
		
	}
	public function add_teacher()
	{
		# code...m
		if (isset($this->session->userdata['login_data'])) {	
		if($this->input->post('submit')){
		$this->form_validation->set_rules('name', 'name', 'required');
		$this->form_validation->set_rules('email', 'email', 'required');

		$this->form_validation->set_rules('password', 'password', 'trim|required|min_length[4]|max_length[22]');

		if ($this->form_validation->run() == TRUE ) {
			# code...
			
			//$data['n_date']= $date; 
			$data = array(
							'name' =>$this->input->post('name'),
							'email'  =>$this->input->post('email'),
							'password' =>$this->input->post('password')
							
							
						);
			$this->db->insert('teacher', $data);
			
			$data['mess']='Sucessfully Enter techer';
			$this->load->view('header');
			$this->load->view('admin/b_teacher',$data);
			$this->load->view('footer');

			
			#work-check in nect phase

		} else {
			# Notice View
			$data['mess']='form validation error';
			$this->load->view('header');
			$this->load->view('admin/b_teacher',$data);
			$this->load->view('footer');
		}
	}
	else{
		# code...
		# Notice View
		$data['mess']='Post error';
			$this->load->view('header');
			$this->load->view('admin/b_teacher',$data);
			$this->load->view('footer');
	}
		}
		else{
			redirect('login/index','refresh');
		}
	}


	public function student()
	{
			if (isset($this->session->userdata['login_data'])) {
			$this->load->view('header');
			$this->load->view('admin/b_student');
			$this->load->view('footer');
		}
		else{
			redirect('login/index','refresh');
		}
	}
	public function add_student()
	{
		# code...m
		if (isset($this->session->userdata['login_data'])) {	
		if($this->input->post('submit')){
		$this->form_validation->set_rules('s_id', 's_id', 'required');
		$this->form_validation->set_rules('class_name', 'class_name', 'required');
		$this->form_validation->set_rules('username', 'username', 'required');
		$this->form_validation->set_rules('password', 'password', 'trim|required|min_length[4]|max_length[22]');

		if ($this->form_validation->run() == TRUE ) {
			# code...
			
			//$data['n_date']= $date; 
			$data = array(
							'class_id' =>$this->input->post('class_name'),
							'email'  =>$this->input->post('username'),
							'password' =>$this->input->post('password'),
							'student_id' =>$this->input->post('s_id')
							
							
						);
			$this->db->insert('student', $data);
			
			$data['mess']='Sucessfully Enter student';
			$this->load->view('header');
			$this->load->view('admin/b_student',$data);
			$this->load->view('footer');

			
			#work-check in nect phase

		} else {
			# Notice View
			$data['mess']='form validation error';
			$this->load->view('header');
			$this->load->view('admin/b_student',$data);
			$this->load->view('footer');
		}
	}
	else{
		# code...
		# Notice View
		$data['mess']='Post error';
			$this->load->view('header');
			$this->load->view('admin/b_student',$data);
			$this->load->view('footer');
	}
		}
		else{
			redirect('login/index','refresh');
		}
	}


	public function vclass()
	{
			if (isset($this->session->userdata['login_data'])) {
			$this->load->view('header');
			$this->load->view('admin/b_class');
			$this->load->view('footer');
		}
		else{
			redirect('login/index','refresh');
		}
		
	}
	public function add_class()
	{
		# code...m
		if (isset($this->session->userdata['login_data'])) {	
		if($this->input->post('submit')){
		$this->form_validation->set_rules('c_name', 'c_name', 'required');
		$this->form_validation->set_rules('c_num', 'c_num', 'required');

		if ($this->form_validation->run() == TRUE ) {
			# code...
			
			//$data['n_date']= $date; 
			$data = array(
							'class_name' =>$this->input->post('c_name'),
							'class_num_id'  =>$this->input->post('c_num')
							
							
						);
			$this->db->insert('class', $data);
			
			$data['mess']='Sucessfully Enter class';
			$this->load->view('header');
			$this->load->view('admin/b_class',$data);
			$this->load->view('footer');

			
			#work-check in nect phase

		} else {
			# Notice View
			$data['mess']='form validation error';
			$this->load->view('header');
			$this->load->view('admin/b_class',$data);
			$this->load->view('footer');
		}
	}
	else{
		# code...
		# Notice View
		$data['mess']='Post error';
			$this->load->view('header');
			$this->load->view('admin/b_class',$data);
			$this->load->view('footer');
	}
}
else{
			redirect('login/index','refresh');
		}
		
	}
	public function notice()
	{
			if (isset($this->session->userdata['login_data'])) {
			$this->load->view('header');
			$this->load->view('admin/notice_body');
			$this->load->view('footer');
		}
		else{
			redirect('login/index','refresh');
		}
		
	}
	public function add_notice()
	{
		# code...
		if (isset($this->session->userdata['login_data'])) {
		if($this->input->post('submit')){
		$this->form_validation->set_rules('n_title', 'n_title', 'required');
		$this->form_validation->set_rules('n_body', 'n_body', 'required');

		if ($this->form_validation->run() == TRUE ) {
			# code...
			$date= date("d-M-Y");
			//$data['n_date']= $date; 
			$data = array(
							'n_title' =>$this->input->post('n_title'),
							'n_body'  =>$this->input->post('n_body'),
							'n_date'  =>$date
							
						);
			$this->db->insert('notice', $data);
			
			$data['mess']='Sucessfully Enter Notice';
			$this->load->view('header');
			$this->load->view('admin/notice_body',$data);
			$this->load->view('footer');

			
			#work-check in nect phase

		} else {
			# Notice View
			$this->load->view('header');
			$this->load->view('admin/notice_body');
			$this->load->view('footer');
		}
	}
	else{
		# code...
		# Notice View
			$this->load->view('header');
			$this->load->view('admin/notice_body');
			$this->load->view('footer');
	}
		
		}
		else{
			redirect('login/index','refresh');
		}
	}

}

/* End of file Admin.php */
/* Location: ./application/controllers/Admin.php */