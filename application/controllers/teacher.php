<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Teacher extends CI_Controller {

public function __construct()
	{
		parent::__construct();
		
		$this->load->library('form_validation');
		$this->load->helper('form');
			
		$this->load->helper('security');
		$this->load->model('user');
		//$this->load->helper('url');
	}

	public function index()
	{
		
	}
	public function login()
	{
		# code...
		if (isset($this->session->userdata['login_tdata'])) {
			redirect('teacher/index','refresh');
	}
	else{
		$this->load->view('teacher/login/login');
	}
	}
	public function chlogin($data)
	{
		$con = "email =". "'" . $data['email'] . "' AND " . "password =" . "'" . $data['password'] . "'";
		$this->db->select('*');
		$this->db->from('teacher');
		$this->db->where($con);
		$this->db->limit(1);
		$query = $this->db->get();

		if($query->num_rows() == 1)
		{
			return true;
		}else
		{
			return false;
		}	
	}
	public function getid($data)
	{
		# code...
	}
	public function logout()
	{
		$sess_array = array(
			'username' => ''
			);
			//$this->session->unset_userdata('login_data', $sess_array);
			 $this->session->unset_userdata('login_tdata');
        //$this->session->sess_destroy();
       // redirect('/user_authentication');
        
        //$this->session->unset_userdata('login_data');
        $this->session->sess_destroy();
			$data['message'] = 'Successfully Logout';
			redirect('teacher/login','refresh');
			
}
	public function assignment()
	{
			$this->load->view('teacher/header');
			$this->load->view('teacher/assignment/b_assigment');
			$this->load->view('teacher/footer');
	}
	public function add_ass()
	{
		if (isset($this->session->userdata['login_data'])) {
		if($this->input->post('submit')){
		$this->form_validation->set_rules('sub_name', 'sub_name', 'required');
		$this->form_validation->set_rules('class_name', 'class_name', 'required');

		$this->form_validation->set_rules('ass_title', 'ass_title', 'required');
		$this->form_validation->set_rules('ass_date', 'ass_date', 'required');
		$this->form_validation->set_rules('ass_link', 'ass_link', 'required');

		if ($this->form_validation->run() == TRUE ) {
			# code...
			$date= date("d-M-Y");
			//$data['n_date']= $date; 
			$data = array(
							'pub_date' =>$date,
							'class_id'  =>$this->input->post('class_name'),
							'sub_id' =>$this->input->post('sub_name'),
							'ass_title'=>$this->input->post('ass_title'),
							'ass_link'=>$this->input->post('ass_link'),
							'last_date'=>$this->input->post('ass_date')
							
							
						);
			$this->db->insert('assignment', $data);
			
			$data['mess']='Sucessfully Enter assignment';
			$this->load->view('header');
			$this->load->view('admin/b_assigment',$data);
			$this->load->view('footer');

			
			#work-check in nect phase

		} else {
			# Notice View
			$data['mess']='form validation error';
			$this->load->view('header');
			$this->load->view('admin/b_assigment',$data);
			$this->load->view('footer');
		}
	}
	else{
		# code...
		# Notice View
		$data['mess']='Post error';
			$this->load->view('header');
			$this->load->view('admin/b_assigment',$data);
			$this->load->view('footer');
	}
		

 	}
 	else{
			redirect('login/index','refresh');
		}
	}
	public function log_process()
	{
		$this->form_validation->set_rules('email', 'email', 'required');
		$this->form_validation->set_rules('password', 'password', 'required');
		if ($this->form_validation->run() == TRUE) {
			# code...
			$data = array(
							'email' =>$this->input->post('email') ,
							'password' =>$this->input->post('password') 
						);
			$result = $this->chlogin($data);

			if($result = TRUEE)
			{
				$session_data = array(
							'username' => $result[0]->username
							
							);

						$this->session->set_userdata('login_tdata', $session_data);

					redirect('teacher/index','refresh');
			}
			else
			{
				$data['message']='invalid password !!';
				$this->load->view('teacher/login/login', $data);
			}

		} else {
			# code...
			$data['message']='Enter Correct data !!';
				$this->load->view('teacher/login/login', $data);
	
		}

	}

}

/* End of file teacher.php */
/* Location: ./application/controllers/teacher.php */