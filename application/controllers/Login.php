<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		
		$this->load->library('form_validation');
		$this->load->helper('form');
			
		$this->load->helper('security');
		$this->load->model('user');
		$this->load->helper('url');
	}

	public function index()
	{
		
		if (isset($this->session->userdata['login_data'])) {
		//$username = ($this->session->userdata['login_data']['fname']);
		//$email = ($this->session->userdata['logged_in']['email']);
						redirect('admin/vclass','refresh');
	
			} 
		$this->load->view('login');
	}
	public function admin()
	{
		# code...
		if (isset($this->session->userdata['login_data'])) {
		$this->load->view('header');
		$this->load->view('b_class');
		$this->load->view('footer');
	}
	else{
		redirect('login/index','refresh');
	}
	}
	public function logout()
	{
		$sess_array = array(
			'username' => ''
			);
			//$this->session->unset_userdata('login_data', $sess_array);
			 $this->session->unset_userdata('login_data');
        //$this->session->sess_destroy();
       // redirect('/user_authentication');
        
        //$this->session->unset_userdata('login_data');
        $this->session->sess_destroy();
			$data['message'] = 'Successfully Logout';
			redirect('login/index','refresh');
			//$this->load->view('login_form', $data);
			//$this->load->view('head');		
			//$this->load->view('nav');
			//$this->load->view('login', $data);
			//$this->load->view('end');
}
	public function login_process()
	{
		# code...
		$this->form_validation->set_rules('email', 'email', 'trim|required');
		$this->form_validation->set_rules('password', 'password', 'trim|required|min_length[5]|max_length[28]');

		if ($this->form_validation->run() == FALSE) {
			if (isset($this->session->userdata['login_data'])) {
		//$username = ($this->session->userdata['login_data']['fname']);
		//$email = ($this->session->userdata['logged_in']['email']);
						redirect('admin/vclass','refresh');
	
				} 
				else
				{
					$data['message']='invalid password !!';
					
					$this->load->view('login', $data);
					
			# code...
				}
			
		} else {
			# code...
			$data = array(
				'email' =>$this->input->post('email') ,
				'password' => $this->input->post('password') 
				);

			$result = $this->user->login($data);
			if($result == TRUE){

				$email = $this->input->post('email');
				$result = $this->user->read_user_info($email);
					if($result != false)
					{
						$session_data = array(
							'username' => $result[0]->username
							
							);

						$this->session->set_userdata('login_data', $session_data);

					redirect('admin/vclass','refresh');

					}

			}
			else
			{
			$data['message']='invalid password !!';
			redirect('login/login_process','refresh');
			
		
			}
		}
	}


}

/* End of file Login.php */
/* Location: ./application/controllers/Login.php */