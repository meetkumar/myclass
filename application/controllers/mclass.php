<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mclass extends CI_Controller {

	public function index()
	{
		$this->load->view('header');
		$this->load->view('admin/b_mclass');
		$this->load->view('footer');		
	}
	public function demo()
	{
		$this->load->view('header');
			$this->load->view('admin/b_msub');
			$this->load->view('footer');
	}
	public function editclass($classid)
	{
		# code...
		$class_id = $classid;
		//echo $class_id;
		$data['class_id'] = $class_id;
		if($class_id != NULL)
		{
			$data['class_id'] = $class_id;
			$this->load->view('header');
			$this->load->view('admin/b_mclass_edit', $data);
			$this->load->view('footer');
		}
	}
	public function del_class($id)
	{
		# code...
		$this->db->where('class_id', $id);
		$this->db->delete('class');
		redirect('mclass/index','refresh');

	}
	public function del_sub($id)
	{
		$this->db->where('sub_id', $id);
		$this->db->delete('subject');
		redirect('mclass/demo','refresh');

	}
	public function class_update_process()
	{
		$this->form_validation->set_rules('id', 'id', 'required');
		$this->form_validation->set_rules('c_name', 'c_name', 'required');
		$this->form_validation->set_rules('c_num', 'c_num', 'required');

		if ($this->form_validation->run() == TRUE) {
			# code...
			if($this->input->post('id')!=NULL)
			{
				$data = array(
							'class_name' =>$this->input->post('c_name'),
							'class_num_id'  =>$this->input->post('c_num')
							
							
						);	
				    $n_id = $this->input->post('id');
					$this->db->where('class_id', $n_id);
					$this->db->update('class', $data);
					redirect('mclass/index','refresh');
			}
		} else {
			# code...
		}

	}





}

/* End of file mclass.php */
/* Location: ./application/controllers/mclass.php */