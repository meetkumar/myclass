<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Getdb extends CI_Model {

	function __construct()
    {
        parent::__construct();
    }
    function get_teachers()
	{
		$query	=$this->db->get('teacher');
		return $query->result_array();
	}
	function get_class()
	{
		$query = $this->db->get('class');
		return $query->result_array();
	}

}

/* End of file Getdb.php */
/* Location: ./application/models/Getdb.php */