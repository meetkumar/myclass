<div class="content-wrapper">
        
        <!-- Content Header (Page header) -->
        
        <!-- Main content -->
        <section class="content">
        <div class="row">
        <div class="col-md-6">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Add Assignment</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
              <?php if (isset($mess)) { ?>
<CENTER><h4 style="color:green;"><?php echo $mess; echo validation_errors(); ?></h4></CENTER><br>
<?php } ?>
                <form role="form" action="<?php echo base_url(); ?>index.php/admin/add_ass" method="post">
                  <div class="box-body">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Class Name</label>
                        <select name="class_name" id="class_name" class="form-control" required="required">
                          <?php $class = $this->db->get('class')->result_array();
                                 foreach($class as $row){?>
                         
                            
                          
                           ?>
                          <option value="<?php echo $row['class_id'];  ?>"><?php echo $row['class_name'];  ?></option>
                          <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                      <label for="exampleInputPassword1">Sub Name</label>
                      <select name="sub_name" id="sub_name" class="form-control" required="required">
                           <?php $class = $this->db->get('subject')->result_array();
                                 foreach($class as $row){
                         
                              $query  = $this->db->get_where('teacher' , array('teacher_id' => $row['teacher_id']))->result_array();
                                 
                                foreach($query as $row1){
                            
                          
                           ?>
                          <option value="<?php echo $row['sub_id'];  ?>"><?php echo $row['name'];  ?>-<?php echo $row1['name']; ?></option>
                          <?php }} ?>
                        </select> </div>
                    
                        <div class="form-group">
                      <label for="exampleInputPassword1">Assignment Title</label>
                      <input type="text" name="ass_title" id="ass_title" class="form-control" id="exampleInputPassword1" placeholder="Title">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputPassword1">Last Date</label>
                      <input type="text" name="ass_date" id="ass_date" class="form-control" id="exampleInputPassword1" placeholder="Last Date">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputPassword1">Link</label>
                      <input type="text" name="ass_link" id="ass_link" class="form-control" id="exampleInputPassword1" placeholder="Link">
                    </div>

                    


                    
                    
                  <div class="box-footer">
                    <button type="submit" name="submit" id="submit" value="submit" class="btn btn-primary">Submit</button>
                  </div>
                </form>
              </div><!-- /.box -->

              <!-- Form Element sizes -->
              
                              </div><!-- /input-group -->
                </div><!-- /.box-body -->
             <!-- /.box -->

            
        
          
        </section><!-- /.content -->

      </div><!-- /.content-wrapper -->