<div class="content-wrapper">
        
        <!-- Content Header (Page header) -->
        
        <!-- Main content -->

<?php 
        if(isset($notice_id))
        {
        $query  = $this->db->get_where('notice' , array('n_id' => $notice_id))->result_array();
                                 
                                foreach($query as $row1){

        ?>   
        <section class="content">
               <div class="box">
            <div class="box-header with-border">
              <h2 class="box-title"><?php echo $row1['n_title']; ?> | </h2>  <?php echo $row1['n_date'];?>
              <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
      
     
      
      <p><?php echo $row1['n_body'];?></p>      
            </div><!-- /.box-body -->
            <div class="box-footer">
           By Admin
            </div><!-- /.box-footer-->
          </div><!-- /.box -->
<?php }}
      else
      {
        echo "No notice Found !!";
      }
      ?>    
       
              <!-- general form elements -->
        
        </section><!-- /.content -->

      </div><!-- /.content-wrapper -->
      