<div class="content-wrapper">
        
        <!-- Content Header (Page header) -->
        
        <!-- Main content -->
        <section class="content">
        <div class="row">
        <div class="col-md-12">
              <!-- general form elements -->
              
    <table class="table table-hover">
    <thead>
      <tr>
        <th>Firstname</th>
        <th>Lastname</th>
        <th>Email</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>John</td>
        <td>Doe</td>
        <td>john@example.com</td>
      </tr>
      <tr>
        <td>Mary</td>
        <td>Moe</td>
        <td>mary@example.com</td>
      </tr>
      <tr>
        <td>July</td>
        <td>Dooley</td>
        <td>july@example.com</td>
      </tr>
    </tbody>
  </table>
              <!-- Form Element sizes -->
              
                              </div><!-- /input-group -->
                </div><!-- /.box-body -->
             <!-- /.box -->

            
        
          
        </section><!-- /.content -->

      </div><!-- /.content-wrapper -->