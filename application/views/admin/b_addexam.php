<div class="content-wrapper">
        
        <!-- Content Header (Page header) -->
        
        <!-- Main content -->
        <section class="content">
        <div class="row">
        <div class="col-md-6">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Add Exam</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
              <?php if (isset($mess)) { ?>
<CENTER><h4 style="color:green;"><?php echo $mess; echo validation_errors(); ?></h4></CENTER><br>
<?php } ?>
                <form role="form" action="<?php echo base_url(); ?>index.php/admin/addexam" method="post">
              <div class="box-body">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Exam Name</label>
                      <input type="text" id="exam_name" name="exam_name" class="form-control" id="exampleInputEmail1" placeholder="Enter exam Name">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Class Name</label>
                        <select name="class_name" id="class_name" class="form-control" required="required">
                        <?php $class = $this->db->get('class')->result_array();
                                 foreach($class as $row){?>
                         
                            
                          
                           ?>
                          <option value="<?php echo $row['class_id'];  ?>"><?php echo $row['class_name'];  ?></option>
                          <?php } ?>
                        </select>
                    </div>
                   

                        <div class="form-group">
                      <label for="exampleInputEmail1">Total marks</label>
                      <input type="text" id="t_marks" name="t_marks" class="form-control" id="exampleInputEmail1" placeholder="Enter Total marks">
                    </div>

                    <div class="form-group">
                      <label for="exampleInputEmail1">Exam Date</label>
                      <input type="text" id="exam_date" name="exam_date" class="form-control" id="exampleInputEmail1" placeholder="Enter Exam date">
                    </div>
                    

                    


                    
                    
                  <div class="box-footer">
                    <button type="submit" name="submit" value="submit" id="submit" class="btn btn-primary">Submit</button>
                  </div>
                </form>
              </div><!-- /.box -->

              <!-- Form Element sizes -->
              
                              </div><!-- /input-group -->
                </div><!-- /.box-body -->
             <!-- /.box -->

            
        
          
        </section><!-- /.content -->

      </div><!-- /.content-wrapper -->