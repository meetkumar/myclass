<div class="content-wrapper">
        
        <!-- Content Header (Page header) -->
        
        <!-- Main content -->
        <section class="content">
        <div class="row">
        <div class="col-md-6">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Add Subject</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
              <?php if (isset($mess)) { ?>
<CENTER><h4 style="color:green;"><?php echo $mess; echo validation_errors(); ?></h4></CENTER><br>
<?php } ?>
                <form role="form" action="<?php echo base_url(); ?>index.php/admin/add_sub" method="post">
              <div class="box-body">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Sub Name</label>
                      <input type="text" id="sub_name" name="sub_name" class="form-control" id="exampleInputEmail1" placeholder="Enter subject Name">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Class Name</label>
                        <select name="class_name" id="class_name" class="form-control" required="required">
                        <?php $class = $this->db->get('class')->result_array();
                                 foreach($class as $row){?>
                         
                            
                          
                           ?>
                          <option value="<?php echo $row['class_id'];  ?>"><?php echo $row['class_name'];  ?></option>
                          <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                      <label for="exampleInputPassword1">Teacher Name</label>
                      <select name="teacher_name" id="teacher_name" class="form-control" required="required">
                          <?php $teacher = $this->db->get('teacher')->result_array();
                                 foreach($teacher as $row){?>
                         
                            
                          
                           ?>
                          <option value="<?php echo $row['teacher_id'];  ?>"><?php echo $row['name']; ?></option>
                          <?php } ?>
                        </select>   </div>
                    

                    


                    
                    
                  <div class="box-footer">
                    <button type="submit" name="submit" value="submit" id="submit" class="btn btn-primary">Submit</button>
                  </div>
                </form>
              </div><!-- /.box -->

              <!-- Form Element sizes -->
              
                              </div><!-- /input-group -->
                </div><!-- /.box-body -->
             <!-- /.box -->

            
        
          
        </section><!-- /.content -->

      </div><!-- /.content-wrapper -->