<div class="content-wrapper">
        
        <!-- Content Header (Page header) -->
        
        <!-- Main content -->
        <section class="content">
        <div class="row">
        <div class="col-md-6">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Notice Info</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <?php if (isset($mess)) { ?>
<CENTER><h4 style="color:green;"><?php echo $mess; echo validation_errors(); ?></h4></CENTER><br>
<?php } ?>
                <form role="form" action="<?php echo base_url(); ?>index.php/admin/add_notice" method="post">
                  <div class="box-body">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Notice Title</label>
                      <input type="text"  name="n_title" class="form-control" id="n_title" placeholder="Enter Title" required>
                    </div>
                    <div class="form-group">
                      <label for="exampleInputPassword1">Notice Body</label>
                      <textarea class="form-control" name="n_body" id="n_body" rows="8" placeholder="Enter Notice" required></textarea>
                      
                    </div>
                    

                    


                    
                    
                  <div class="box-footer">
                    <button type="submit" value="submit" name="submit" id="submit" class="btn btn-primary">Submit</button>
                  </div>
                </form>
              </div><!-- /.box -->

              <!-- Form Element sizes -->
              
                              </div><!-- /input-group -->
                </div><!-- /.box-body -->
             <!-- /.box -->

            
        
          
        </section><!-- /.content -->

      </div><!-- /.content-wrapper -->